import React from 'react'

const Picture = ({photo}) => {
    const {urls, description} = photo
  return (
    <div>
        <img src={urls.small} alt={description}/>
    </div>
  )
}

export default Picture