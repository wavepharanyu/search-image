import { useState } from "react";
import axios from "axios";
import "./App.css";
import Picture from "./components/Picture";

function App() {
  const [word, setWord] = useState("");
  const [photos, setPhotos] = useState([]);

  const searchImage = (e) => {
    e.preventDefault();
    if (!word) {
      alert("กรุณาป้อนข้อมูล");
    } else {
      fetchImageAPI();
    }
  };

  const fetchImageAPI = async () => {
    const url = `${import.meta.env.VITE_API_URL}?page=1&query=${word}&client_id=${import.meta.env.VITE_API_KEY}&per_page=15`;
    try {
      const res = await axios.get(url);
      const data = res.data.results;
      if (data.length === 0) {
        alert("ไม่มีข้อมูลรูปภาพ");
        setWord("");
      } else {
        setPhotos(data);
      }
    } catch (error) {
      throw error;
    }
  };

  return (
    <>
      <h1>ระบบค้นหารูปภาพด้วย API</h1>
      <form onSubmit={searchImage}>
        <input
          type="text"
          placeholder="ป้อนชื่อรูปภาพ"
          value={word}
          onChange={(e) => setWord(e.target.value)}
        />
        <button type="submit">ค้นหา</button>
      </form>
      <div className="search-result">
        {photos.map((photo, index) => {
          return <Picture photo={photo} key={index} />;
        })}
      </div>
    </>
  );
}

export default App;
